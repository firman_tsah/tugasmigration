<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
  <div class="container">
    <h1>selamat datang di form edit</h1>
    <form role="form" action="/pertanyaan/{{$post->id}}" method="POST">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label for="exampleInputEmail1">Judul pertanyaan</label>
        <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $post->judul)}}">
        <small id="emailHelp" class="form-text text-muted">silahkan rubah judul pertanyaan anda.</small>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Isi pertanyaan</label>
        <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi', $post->isi)}}">
        <small id="emailHelp" class="form-text text-muted">silahkan rubah pertanyaan anda.</small>
      </div>
      <button type="submit" class="btn btn-primary">Selesai</button>
    </form>
  </div>
</body>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</html>