<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container">
    <h1>selamat datang</h1>
    @if(session('berhasil'))
    <div class="alert alert-primary" role="alert">
        {{ session('berhasil')}}
      </div>
    @endif
    <a type="button" class="btn btn-primary btn-lg btn-block mb-2" href="/pertanyaan/create"><h2>Buat pertanyaan</h2></a>
    <table class="table">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Judul Pertanyaan</th>
      <th scope="col">Isi pertanyaan</th>
      <th scope="col" style="width: 50px" >Action</th>
    </tr>
  </thead>
  <tbody>
        @foreach ($pertanyaan as $key => $item)
            <tr>
                <td> {{ $key+1 }}</td>
                <td> {{ $item->judul }}</td>
                <td> {{ $item->isi}}</td>
                <td style="display: flex">
                    <a href="/pertanyaan/{{$item->id}}" class="btn btn-info btn-sm">Lihat</a>
                    <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-warning btn-sm">Ubah</a>
                    <form action="/pertanyaan/{{$item->id}}" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @endforeach
  </tbody>
</table>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</html>