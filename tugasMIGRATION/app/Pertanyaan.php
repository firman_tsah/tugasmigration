<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PertanyaanModel extends Model
{
    protected $table = 'pertanyaan';
    protected $primaryKey = 'id' ;
    protected $fillable = [
                            'judul',
                            'isi'
    ];
}
