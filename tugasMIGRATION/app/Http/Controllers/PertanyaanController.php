<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index()
    {
        $pertanyaan = DB::table('pertanyaan')->get();
        // dd($pertanyaan);
        return view('pertanyaan.index', compact('pertanyaan'));
    }
    public function create()
    {
        return view('pertanyaan.create');
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $query = DB::table('pertanyaan')->insert([
            "judul" =>$request["judul"],
            "isi" =>$request["isi"]
        ]);
            return redirect('/pertanyaan')->with('berhasil', 'pertanyaan berhasil di buat');
    }
    public function show($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
            // dd($post);
            return view('pertanyaan.show', compact('post'));
    }
    public function edit($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
            // dd($post);
            return view('pertanyaan.edit', compact('post'));
    }
    public function update($id, Request $request){
        $query =DB::table('pertanyaan')
                ->where('id', $id)
                ->update([
                    'judul' => $request['judul'],
                    'isi' => $request['isi']
                ]);
            return redirect('pertanyaan')->with('berhasil', 'berhasil di perbaharui');
    }
    public function destroy($id){
        $query =DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('pertanyaan')->with('berhasil', 'pertanyaan berhasil di hapus');

    }



}

